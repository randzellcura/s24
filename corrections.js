const get_cube = 2 ** 3
console.log(`The cube of 2 is ${get_cube}`)

const address = ["20", "Pinaglabanan St.", "San Juan", "1008"]
// Destructure
const [house_number, street, city, zip_code] = address
console.log(`I live at ${house_number} ${street}, ${city} ${zip_code}`)

const animal = {
    name: "Lolong",
    species: "Salwater Crocodile",
    weight: "1075 kg",
    measurement: "20 ft 3 in"
}
const { name, species, weight, measurement } = animal
console.log(`${name} was a ${species.toLowerCase()}. He weighed at ${weight}s with a measurement of ${measurement}`)

let numbers = [1, 2, 3, 4, 5]
numbers.forEach((number) => console.log(number))

let reduce_number = numbers.reduce((x, y) => x + y )
console.log(reduce_number)