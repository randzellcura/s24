
let num = 2
let getCube = Math.pow(num, 3)

console.log(`The cube of ${num} is ${getCube}`)

const address = ["256 Washington Ave NW", "California", "90011"]
console.log(`I live at ${address[0]}, ${address[1]} ${address[2]}`)

const lolong = [1075, 20, 3]
lolong.forEach(() => {
    console.log(`Lolong was a saltwater crocodile. He weighed at ${lolong[0]} kgs with a measurement of ${lolong[1]} ft ${lolong[2]} in.`)
})

const array = [1, 2, 3, 4, 5, 15] 

array.forEach((number) => {
    console.log(number);
})

const animal = {
    name: "Frankie",
    age: 5,
    breed: "Miniature Dachshung"
}

console.log(animal)

// Corrections or other alternatives in corrections.js

// Template Literals `` ${}